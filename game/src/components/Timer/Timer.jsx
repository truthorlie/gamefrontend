import React, {useState, useEffect} from "react";
import './Timer.css'

export default function Timer() {
    const [seconds, setSeconds] = useState(30);
    const isActive = true;

    useEffect(() => {
        let interval;

        if (isActive && seconds > 0) {
            interval = setInterval(() => {
                setSeconds(prevSeconds => prevSeconds - 1);
            }, 1000);
        }

        return () => clearInterval(interval);
    }, [isActive, seconds]);

    return (
        <div className="timer">
            {seconds} сек
        </div>
    );
}