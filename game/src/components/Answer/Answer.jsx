import Button from "../Button/Button"
import './Answer.css'

export default function Answer() {
    const buttonStyleYes = {
        fontSize: '30px',
        transitionDuration: '0.4s',
        cursor: 'pointer',
        textAlign: 'center',
        border: 'none',
        textDecoration: 'none',
        display: 'inline-block',
        padding: '20px 15px',
        backgroundColor: '#93bd08',
        borderRadius: '50px'
    }

    const buttonStyleNo = {
        fontSize: '30px',
        transitionDuration: '0.4s',
        cursor: 'pointer',
        textAlign: 'center',
        border: 'none',
        textDecoration: 'none',
        display: 'inline-block',
        padding: '20px 18px',
        backgroundColor: '#ff173a',
        borderRadius: '36px'
    }

    return (
        <div className="answer">
            <div className="button-yes">
                <Button title={'Yes'} style={buttonStyleYes}/>
            </div>
            <div className="button-no">
                <Button title={'No'} style={buttonStyleNo}/>
            </div>
        </div>
    )
}