import React, { useState, useEffect } from 'react';
import axios from 'axios';

class Data extends React.Component{
    state = { details: [],}

    componentDidMount(){
        let data;
        axios.get('http://127.0.0.1:8000/questions/api/list/')
        .then(res => {
            data = res.data;
            this.setState({
                details: data
            })
        })
        .catch(err => {
            console.log(err)
        })
    }
    
    render() {
        const dataJson = this.state.details[0]
        console.log(dataJson)
        return (
            
            
            <div>
                <hr></hr>
                {this.state.details.map((output, id) => (
                    <div key={id}>
                        {output.title}
                    </div>
                ))}
            </div>
        )
    }
}

export default Data