// import './Button.css'

export default function Button({title, style}) {
    

    return (
        <button className="button" style={{...style}}>{title}</button>
    )
}