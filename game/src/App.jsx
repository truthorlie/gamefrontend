import './App.css'
import Answer from './components/Answer/Answer'
import Counter from './components/Counter/Counter'
import Question from './components/Question/Question'
import Timer from './components/Timer/Timer'
import Data from './components/Data'

export default function App() {
  return (
    <>
      <div className='container'>
        <div className='group-1'>
          <Timer />
          <Counter />
        </div>
        <Question />
        <Answer />
      </div>
    </>
  )
}

